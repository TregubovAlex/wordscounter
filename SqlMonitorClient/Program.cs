﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SqlMonitorClient {
    class Program {
        private static Thread monitorThread;
        static void Main(string[] args) {

            monitorThread=new Thread(MonitorCycle);
            monitorThread.Name = "monitorThread";
            monitorThread.IsBackground = true;
            monitorThread.Start();

            Console.ReadKey();
        }

        private static void MonitorCycle() {
            var client = new WordCounter_ServiceReference.ServiceClient("BasicHttpBinding_IService");
            Console.CursorVisible = false;
            while (true) {
                string text = client.GetCurrentText();
                int? result = client.GetCurrentResult();
                Console.Clear();
                Console.WriteLine("Клиент-монитор базы SQL");
                Console.WriteLine("=======================");
                Console.WriteLine();
                Console.WriteLine("Текущий текст в базе:");
                Console.WriteLine(text);
                Console.WriteLine();
                Console.WriteLine("Последний сохранённый в базе результат: {0}", result);
                Console.WriteLine();
                Thread.Sleep(1000);
            }
        }
    }
}