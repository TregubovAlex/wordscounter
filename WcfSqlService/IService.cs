﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfSqlService {
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IService" в коде и файле конфигурации.
    [ServiceContract]
    public interface IService {

        [OperationContract]
        string GetCurrentText();

        [OperationContract]
        int? GetCurrentResult();

        [OperationContract]
        void UpdateResult(int _result);

    }
}
