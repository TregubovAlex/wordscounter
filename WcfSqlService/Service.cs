﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfSqlService {
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service" в коде и файле конфигурации.
    public class Service : IService {

        DatabaseEntities context = new DatabaseEntities();

        public string GetCurrentText() {
            return context.Inputs.FirstOrDefault() != null ? context.Inputs.FirstOrDefault().Text : null;
        }

        public int? GetCurrentResult() {
            return context.Outputs.FirstOrDefault() != null ? context.Outputs.FirstOrDefault().WordCount : null;
        }

        public void UpdateResult(int _result) {
            if (context.Outputs.FirstOrDefault() != null)
                context.Outputs.FirstOrDefault().WordCount = _result;
            else
                context.Outputs.Add(new Output { Id = 1, WordCount = _result });
            context.SaveChanges();
        }
    }
}