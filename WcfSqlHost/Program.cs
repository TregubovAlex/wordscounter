﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfSqlHost {
    class Program {
        static void Main(string[] args) {
            using (ServiceHost host = new ServiceHost(typeof(WcfSqlService.Service))) {
                host.Open();
                Console.WriteLine("Хост сервиса доступа к SQL запущен.");
                Console.ReadKey();
            }
        }
    }
}
