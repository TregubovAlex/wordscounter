﻿using System.Linq;

namespace WordCounter {
    class WordCounter {

        IReader reader;

        public WordCounter(IReader _reader) {
            reader = _reader;
        }

        public int Count() {
            return reader.GetText().Split(' ').Count();
            //Согласно задания метод не учитывает множество ситуаций (например, пустая строка, отсутствие пробелов после знаков препинания и т.п.)
        }

    }
}