﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter {

    /// <summary>
    /// Важно: В констукторах класса реализующего данный интерфейс 
    /// обязательно нужно присвоить значение свойству SourceName, 
    /// содержащее краткое и понятное название источника текста!
    /// Требование связано с рефлекторным механизмом получения списка источников текста.
    /// </summary>
    interface IReader {

        /// <summary>
        /// Краткое и понятное название источника текста.
        /// Используется для вывода на экран пользоввателю.
        /// </summary>
        string SourceName { get; set; }

        /// <summary>
        /// Метод получения текста.
        /// </summary>
        /// <returns></returns>
        string GetText();

    }
}