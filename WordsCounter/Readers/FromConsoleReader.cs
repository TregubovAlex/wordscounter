﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter {
    class FromConsoleReader : IReader {

        public string SourceName { get; set; }

        public FromConsoleReader() {
            SourceName = "с консоли";
        }

        public string GetText() {
            Console.WriteLine("Введите текст:");
            return Console.ReadLine();
        }

    }
}