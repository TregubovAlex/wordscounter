﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter {
    class FromFileReader : IReader {

        string _inputFileName = "input.txt";

        public string SourceName { get; set; }

        public FromFileReader() {
            SourceName = "из файла " + _inputFileName;
        }

        public string GetText() {
            return File.ReadAllText(Environment.CurrentDirectory + "\\" + _inputFileName, Encoding.Default);
        }

    }
}