﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;

namespace WordCounter {
    public class FromSqlReader : IReader {

        public string SourceName { get; set; }

        public FromSqlReader() {
            SourceName = "из базы данных SQL";
        }

        public string GetText() {
            var client = new WordCounter_ServiceReference.ServiceClient("BasicHttpBinding_IService");
            string text = client.GetCurrentText();
            client.Close();
            return text;
        }

        public int? GetCurrentResult() {
            var client = new WordCounter_ServiceReference.ServiceClient("BasicHttpBinding_IService");
            int? result = client.GetCurrentResult();
            client.Close();
            return result;
        }
    }
}