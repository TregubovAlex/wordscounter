﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter {
    class ToFileWriter : IWriter {

        string _outputFileName = "output.txt";

        public string SourceName { get; set; }

        public ToFileWriter() {
            SourceName = "в файл " + _outputFileName;
        }

        public void WriteResult(int _result) {
            File.WriteAllText(Environment.CurrentDirectory + "\\" + _outputFileName, _result.ToString(), Encoding.Default);
        }
    }
}