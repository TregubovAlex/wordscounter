﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter {
    class ToSqlWriter : IWriter {

        public string SourceName { get; set; }

        public ToSqlWriter() {
            SourceName = "в базу данных SQL";
        }

        public void WriteResult(int _result) {
            var client = new WordCounter_ServiceReference.ServiceClient("BasicHttpBinding_IService");
            client.UpdateResult(_result);
            client.Close();
        }
    }
}