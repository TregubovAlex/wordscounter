﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter {

    /// <summary>
    /// Важно: В констукторах класса реализующего данный интерфейс 
    /// обязательно нужно присвоить значение свойству SourceName, 
    /// содержащее краткое и понятное название варианта для вывода результата!
    /// Требование связано с рефлекторным механизмом получения списка вариантов для вывода результата.
    /// </summary>
    interface IWriter {

        /// <summary>
        /// Краткое и понятное название объекта для вывода текста.
        /// Используется для вывода на экран пользоввателю.
        /// </summary>
        string SourceName { get; set; }

        /// <summary>
        /// Метод сохранения результата.
        /// </summary>
        void WriteResult(int _result);

    }
}