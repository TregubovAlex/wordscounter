﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter {
    class EmptyWriter : IWriter {
        
        public string SourceName { get; set; }

        public EmptyWriter() {
            SourceName = "";
        }

        public void WriteResult(int _result) {
        }

    }
}