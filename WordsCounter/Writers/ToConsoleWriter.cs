﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter {
    class ToConsoleWriter : IWriter {

        public string SourceName { get; set; }

        public ToConsoleWriter() {
            SourceName = "на консоль";
        }

        public void WriteResult(int _result) {
            Console.WriteLine(_result.ToString());
        }
    }
}