﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace WordCounter {
    class Program {
        static void Main(string[] args) {
            do {
                Console.Clear();
                Console.WriteLine("Варианты ввода:");
                List<Type> readers = GetInteractionVariants(typeof (IReader));
                ShowInteractionVariants(readers);

                Console.Write("Введите номер варианта ввода текста: ");
                int input = int.Parse(Console.ReadLine());
                IReader reader = (IReader) Activator.CreateInstance(readers[input]);
                WordCounter wordCounter = new WordCounter(reader);

                int wordCount = wordCounter.Count();
                Console.WriteLine();
                Console.WriteLine("Подсчёт окончен.");
                Console.WriteLine();



                Console.WriteLine("Варианты вывода:");
                List<Type> writers = GetInteractionVariants(typeof (IWriter));
                ShowInteractionVariants(writers);

                Console.Write("Введите номер варианта вывода результата: ");
                input = int.Parse(Console.ReadLine());
                IWriter writer = (IWriter) Activator.CreateInstance(writers[input]);
                writer.WriteResult(wordCount);

                Console.WriteLine("Готово!");
                Console.Write("Нажмите любую клавишу... (для выхода - Escape)");
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }

        private static List<Type> GetInteractionVariants(Type _targetType) {
            return Assembly.GetAssembly(_targetType)
                           .GetTypes()
                           .Where(t => t.GetInterface(_targetType.FullName) != null)
                           .ToList();
        }

        private static void ShowInteractionVariants(List<Type> _list) {
            int i = 1;
            foreach (Type item in _list) {
                string SourceName = item.GetProperty("SourceName").GetValue(Activator.CreateInstance(item)).ToString();
                if (SourceName != "")
                    Console.WriteLine("{0}: {1}", i++, SourceName);
            }
        }
    }
}