/*
Задание:
Необходимо написать небольшое консольное приложение (.NET, C#) которое:
Подсчитывает количество слов в тексте
Имеет 3 варианта ввода данных: консоль, файл, база данных SQL.
Имеет 3 варианта вывода результата: консоль, файл, база данных SQL.

Пожалуйста, сфокусируйтесь на архитектуре, она должна быть гибкой и поддерживать различные типы ввода, вывода и обработки текста.
Сам подсчёт слов может содержать ошибки.
 
Вы можете использовать любые сторонние библиотеки, какие посчитайте нужным.
Жёсткого ограничения по времени нет.
*/